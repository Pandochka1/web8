$(document).ready(function(){
    FormClose();
});
function FormOpen(){
    $("#popup").show();
    history.pushState(null,'form','./form'); // добавляется в ссылку страницы слэш форма 
    $("#name").val(localStorage.getItem('name'));
    $("#email").val(localStorage.getItem('email'));
    $("#message").val(localStorage.getItem('message'));

}

function FormClose(){
    $("#popup").hide();
    history.replaceState({id: null}, 'Default state', './');
}
window.addEventListener('popstate', function() {  // обработчик события 
	FormClose();
});